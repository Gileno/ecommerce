# coding=utf-8

from django.apps import AppConfig


class CoreAppConfig(AppConfig):

    verbose_name = u'Comum'
    label = 'core'
    name = 'apps.core'


default_app_config = u'apps.core.CoreAppConfig'
