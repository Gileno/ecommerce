# coding=utf-8

from django.contrib import admin

from .models import Contact


class ContactAdmin(admin.ModelAdmin):

    list_display = ['name', 'email', 'subject', 'created']
    search_fields = ['name', 'email', 'subject']
    list_filter = ['created']


admin.site.register(Contact, ContactAdmin)
