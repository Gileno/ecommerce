# coding=utf-8

from django.conf.urls import url

from .views import about, contact, index


urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^sobre/$', about, name='about'),
    # url(
    #     r'^$', render_template,
    #     {'template_name': 'index.html'}, name='index'
    # ),
    # url(
    #     r'^sobre/$', render_template,
    #     {'template_name': 'about.html'}, name='about'
    # ),
    url(r'^contato/$', contact, name='contact'),
]
