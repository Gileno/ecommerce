# coding=utf-8

from django.db import models
from django.conf import settings

from ecommerce.utils.mail import send_mail_template


class Contact(models.Model):

    name = models.CharField(u'Nome', max_length=50)
    email = models.EmailField(u'E-mail')
    subject = models.CharField(u'Assunto', max_length=100)
    message = models.TextField(u'Mensagem')

    created = models.DateTimeField(
        u'Criado em', auto_now_add=True
    )

    def send_mail(self):
        context_email = {
            'message': self.message, 'name': self.name,
            'email': self.email, 'date': self.created
        }
        send_mail_template(
            self.subject, [settings.CONTACT_EMAIL],
            'contact_email.html', context_email
        )

    def __unicode__(self):
        return self.subject

    class Meta:
        verbose_name = u'Contato'
        verbose_name_plural = u'Contatos'


def contact_post_save(**kwargs):
    contact = kwargs['instance']
    contact.send_mail()

models.signals.post_save.connect(
    contact_post_save, sender=Contact
)
