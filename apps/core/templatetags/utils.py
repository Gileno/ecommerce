# coding=utf-8

from django import template


register = template.Library()


@register.filter
def remove_and_encode(querydict, key):
    copy = querydict.copy()
    if key in copy:
        del copy[key]
    return copy.urlencode()
