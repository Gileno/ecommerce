# coding=utf-8

from django.shortcuts import render, get_object_or_404
from django.conf import settings
from django.views import generic
from django.http import Http404

from ecommerce.utils.mail import send_mail_template

from apps.catalog.models import Product, Category

from .models import Contact
from .forms import ContactForm


class IndexView(generic.ListView):

    def get_paginate_by(self, queryset):
        return self.request.GET.get('paginate_by', 3)

    def get_queryset(self):
        pk = self.request.GET.get('category')
        try:
            category = Category.objects.get(pk=pk)
        except:
            category = None
        if category:
            return category.products.all()
        else:
            return Product.objects.all()

    def get_template_names(self):
        if self.request.is_ajax():
            return ['_index.html']
        else:
            return ['index.html']


index = IndexView.as_view()


def about(request):
    context = {}
    return render(request, 'about.html', context)

def contact(request):
    context = {}
    form = ContactForm(request.POST or None)
    if form.is_valid():
        contact = form.save()
        context['success'] = True
    context['form'] = form
    return render(request, 'contact.html', context)
