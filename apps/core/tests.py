# coding=utf-8

from django.test import TestCase, Client
from django.core.urlresolvers import reverse
from django.core import mail

from model_mommy import mommy

from apps.catalog.models import Product, Category

from .models import Contact


class ContactViewTestCase(TestCase):

    def setUp(self):
        self.contact_url = reverse('core:contact')
        self.client = Client()

    def tearDown(self):
        Contact.objects.all().delete()

    def test_form_success(self):
        data = {
            'name': 'Gileno', 'email': 'g@g.com',
            'subject': 'Assunto', 'message': 'Mensagem'
        }
        response = self.client.post(self.contact_url, data)
        self.assertEquals(response.status_code, 200)
        self.assertEquals(Contact.objects.count(), 1)
        self.assertEquals(len(mail.outbox), 1)
        self.assertEquals(mail.outbox[0].subject, 'Assunto')

    def test_form_error(self):
        data = {'name': 'Gileno'}
        response = self.client.post(self.contact_url, data)
        self.assertEquals(response.status_code, 200)
        self.assertEquals(Contact.objects.count(), 0)
        self.assertEquals(len(mail.outbox), 0)
        self.assertFormError(
            response, 'form', 'email',
            u'Este campo é obrigatório.'
        )
        self.assertFormError(
            response, 'form', 'subject',
            u'Este campo é obrigatório.'
        )
        self.assertFormError(
            response, 'form', 'message',
            u'Este campo é obrigatório.'
        )

    def test_template_used(self):
        response = self.client.get(self.contact_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'contact.html')


class IndexViewTestCase(TestCase):

    def setUp(self):
        self.client = Client()
        self.url_index = reverse('core:index')
        mommy.make(Product, name='Produto', _quantity=2)

    def tearDown(self):
        Product.objects.all().delete()

    def test_products_in_context(self):
        response = self.client.get(self.url_index)
        products = response.context['products']
        self.assertEqual(products.count(), 2)

    def test_template_used(self):
        response = self.client.get(self.url_index)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'index.html')
