# coding=utf-8

from django.apps import AppConfig


class CatalogAppConfig(AppConfig):

    verbose_name = u'Catálogo'
    label = 'catalog'
    name = 'apps.catalog'


default_app_config = u'apps.catalog.CatalogAppConfig'
