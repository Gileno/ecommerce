# coding=utf-8

from django import template

from apps.catalog.models import Category


register = template.Library()


@register.inclusion_tag('catalog/load_categories.html')
def load_categories():
    return {
        'categories': Category.objects.all()
    }


@register.assignment_tag
def get_categories():
    return Category.objects.all()
