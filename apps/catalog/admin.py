# coding=utf-8

from django.contrib import admin

from .models import Category, Product, Photo


class CategoryAdmin(admin.ModelAdmin):

    list_display = ['name', 'slug', 'created', 'modified']
    search_fields = ['name', 'slug']
    list_filter = ['created', 'modified']
    prepopulated_fields = {'slug': ['name']}


class PhotoInline(admin.StackedInline):

    model = Photo
    extra = 1


class ProductAdmin(admin.ModelAdmin):

    list_display = [
        'name', 'slug', 'category_href', 'created', 'modified'
    ]
    search_fields = ['name', 'slug', 'category__name']
    list_filter = ['created', 'modified']
    prepopulated_fields = {'slug': ['name']}
    inlines = [
        PhotoInline
    ]


class PhotoAdmin(admin.ModelAdmin):

    list_display = ['product', 'image', 'primary']
    search_fields = ['product__name']


# admin.site.register([Category, Product])
admin.site.register(Category, CategoryAdmin)
admin.site.register(Product, ProductAdmin)
admin.site.register(Photo, PhotoAdmin)
