# coding=utf-8

from django.shortcuts import render, get_object_or_404
from django.http import Http404
from django.core import exceptions
from django.views import generic

from .models import Category, Product


class ProductDetailView(generic.DetailView):

    template_name = 'catalog/product_detail.html'
    model = Product


class ProductListView(generic.ListView):

    template_name = 'catalog/category_detail.html'
    paginate_by = 9
    context_object_name = 'products'

    def category():
        doc = "The category property."
        def fget(self):
            if not hasattr(self, '_category'):
                self._category = get_object_or_404(
                    Category, slug=self.kwargs['slug']
                )
            return self._category
        return locals()
    category = property(**category())

    def get_queryset(self):
        return self.category.products.all()

    def get_context_data(self, **kwargs):
        context = super(ProductListView, self).get_context_data(
            **kwargs
        )
        context['category'] = self.category
        return context
