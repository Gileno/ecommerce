# coding=utf-8

from django.conf.urls import url

from .views import ProductDetailView, ProductListView


urlpatterns = [
    url(
        r'^produtos/(?P<slug>[\w_-]+)/$',
        ProductDetailView.as_view(),
        name='product_detail'
    ),
    url(
        r'^categorias/(?P<slug>[\w_-]+)/$',
        ProductListView.as_view(),
        name='category_detail'
    ),
]
