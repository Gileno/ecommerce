# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Photo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image', models.ImageField(upload_to=b'photos', verbose_name='Imagem')),
                ('primary', models.BooleanField(default=False, verbose_name='Prim\xe1ria?')),
                ('product', models.ForeignKey(related_name='photos', verbose_name='Produto', to='catalog.Product')),
            ],
            options={
                'ordering': ['-primary'],
                'verbose_name': 'Foto',
                'verbose_name_plural': 'Fotos',
            },
        ),
    ]
