# coding=utf-8

from django.db import models
from django.core.urlresolvers import reverse


class Category(models.Model):

    name = models.CharField(u'Nome', max_length=100)
    slug = models.SlugField(u'Identificador', max_length=100)

    # audit fields
    created = models.DateTimeField(
        u'Criado em', auto_now_add=True
    )
    modified = models.DateTimeField(
        u'Modificado em', auto_now=True
    )

    def get_absolute_url(self):
        return reverse(
            'catalog:category_detail', args=[self.slug]
        )

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = u'Categoria'
        verbose_name_plural = u'Categorias'
        ordering = ['name']


class Product(models.Model):

    name = models.CharField(u'Nome', max_length=100)
    slug = models.SlugField(u'Identificador', max_length=100)
    description = models.TextField(u'Descrição', blank=True)
    category = models.ForeignKey(
        Category, verbose_name=u'Categoria',
        related_name='products'
    )
    price = models.DecimalField(
        u'Preço', decimal_places=2, max_digits=10, default=0
    )

    # audit fields
    created = models.DateTimeField(u'Criado em', auto_now_add=True)
    modified = models.DateTimeField(u'Modificado em', auto_now=True)

    def category_href(self):
        return u'<a href="{}">{}</a>'.format(
            reverse(
                'admin:catalog_category_change',
                args=[self.category_id]
            ),
            self.category.name
        )
    category_href.allow_tags = True
    category_href.short_description = u'Categoria'

    def primary_photo(self):
        return self.photos.first()

    def get_absolute_url(self):
        return reverse(
            'catalog:product_detail', args=[self.slug]
        )

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = u'Produto'
        verbose_name_plural = u'Produtos'
        ordering = ['name']


class Photo(models.Model):

    product = models.ForeignKey(
        Product, verbose_name=u'Produto', related_name='photos'
    )
    image = models.ImageField(
        u'Imagem', upload_to='photos'
    )
    primary = models.BooleanField(u'Primária?', default=False)

    def __unicode__(self):
        return self.image.name.split('/')[-1]

    class Meta:
        verbose_name = u'Foto'
        verbose_name_plural = u'Fotos'
        ordering = ['-primary']
