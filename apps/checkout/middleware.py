# coding=utf-8

import uuid


class CartItemMiddleware(object):

    def process_request(self, request):
        if 'cart_id' not in request.session:
            request.session['cart_id'] = str(uuid.uuid4())
