# coding=utf-8

from django.shortcuts import (
    render, get_object_or_404, redirect
)
from django.contrib import messages

from apps.catalog.models import Product

from .models import CartItem


def add_cartitem(request, pk):
    product = get_object_or_404(Product, pk=pk)
    cartitem, created = CartItem.objects.get_or_create(
        product=product, cart_id=request.session['cart_id']
    )
    cartitem.quantity = cartitem.quantity + 1
    cartitem.save()
    messages.success(
        request, u'Produto adicionado com sucesso'
    )
    return redirect(product.get_absolute_url())
