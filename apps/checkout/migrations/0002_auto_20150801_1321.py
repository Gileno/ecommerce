# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('checkout', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cartitem',
            name='cart_id',
            field=models.CharField(default=uuid.uuid4, max_length=100, verbose_name='Chave do Carrinho'),
        ),
    ]
