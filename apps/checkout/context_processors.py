# coding=utf-8

from .models import CartItem


def cart_items(request):
    return  {
        'cart_items': CartItem.objects.cart(
            request.session['cart_id']
        )
    }
