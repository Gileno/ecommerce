# coding=utf-8

from django.conf.urls import url

from .views import add_cartitem


urlpatterns = [
    url(
        r'^adicionar-carrinho/(?P<pk>\d+)/$', add_cartitem,
        name='add_cartitem'
    ),
]
