# coding=utf-8

import uuid

from django.db import models


class CartItemManager(models.Manager):

    def cart(self, cart_id):
        return self.filter(cart_id=cart_id)


class CartItem(models.Model):

    cart_id = models.CharField(
        u'Chave do Carrinho', max_length=100,
        default=uuid.uuid4
    )
    product = models.ForeignKey(
        'catalog.Product', verbose_name=u'Produto',
        related_name='items'
    )
    quantity = models.PositiveIntegerField(
        u'Quantidade', default=0, blank=True
    )

    created = models.DateTimeField(
        u'Criado em', auto_now_add=True
    )
    modified = models.DateTimeField(
        u'Modificado em', auto_now=True
    )

    objects = CartItemManager()

    def __unicode__(self):
        return u'{0} ({1})'.format(self.product, self.quantity)

    class Meta:
        verbose_name = u'Item do Carrinho'
        verbose_name_plural = u'Itens dos Carrinhos'
