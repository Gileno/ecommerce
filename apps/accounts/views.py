# coding=utf-8

from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import PasswordChangeForm

from .forms import EditUserForm


@login_required
def index(request):
    template_name = 'accounts/index.html'
    form = EditUserForm(
        request.POST or None, instance=request.user
    )
    if form.is_valid():
        form.save()
    context = {
        'form': form
    }
    return render(request, template_name, context)


@login_required
def password_change(request):
    template_name = 'accounts/password_change.html'
    form = PasswordChangeForm(
        user=request.user, data=request.POST or None
    )
    if form.is_valid():
        form.save()
        form = PasswordChangeForm(user=request.user)
    context = {
        'form': form
    }
    return render(request, template_name, context)








"""
"""
