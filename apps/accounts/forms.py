# coding=utf-8

from django import forms
from django.conf import settings
from django.contrib.auth import get_user_model


User = get_user_model()


class EditUserForm(forms.ModelForm):

    # def clean(self):
    #     raise forms.ValidationError(u'Erro geral')

    def clean_email(self):
        email = self.cleaned_data['email']
        query = User.objects.filter(email=email).exclude(
            pk=self.instance.pk
        )
        if email and query.exists():
            raise forms.ValidationError(
                u'Já existe um usuário com este e-mail'
            )
        return email

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email']
