# coding=utf-8

from django.conf.urls import url

from .views import index, password_change


urlpatterns = [
    url(
        r'^entrar/', 'django.contrib.auth.views.login',
        {'template_name': 'accounts/login.html'},
        name='login'
    ),
    url(
        r'^sair/', 'django.contrib.auth.views.logout',
        {'next_page': 'core:index'},
        name='logout'
    ),
    url(r'^$', index, name='index'),
    url(
        r'^alterar-senha/$', password_change, 
        name='password_change'
    ),
]
