# coding=utf-8

from django.test import TestCase, Client
from django.core.urlresolvers import reverse
from django.contrib.auth import get_user_model

from model_mommy import mommy


User = get_user_model()


class IndexViewTestCase(TestCase):

    def test_login_required(self):
        response = self.client.get(self.index_url)
        self.assertEquals(response.status_code, 302)
        self.client.login(
            username=self.user.username, password='123'
        )
        response = self.client.get(self.index_url)
        self.assertEquals(response.status_code, 200)

    def test_template_used(self):
        self.client.login(
            username=self.user.username, password='123'
        )
        response = self.client.get(self.index_url)
        self.assertTemplateUsed(
            response, 'accounts/index.html'
        )

    def setUp(self):
        self.user = mommy.prepare(User)
        self.user.set_password('123')
        self.user.save()
        self.index_url = reverse('accounts:index')
        self.client = Client()

    def tearDown(self):
        self.user.delete()
