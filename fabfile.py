# -*- coding: utf-8 -*-

import os
import time

from fabric.contrib.console import confirm
from fabric.api import (
    local, settings as fabric_settings, env, cd, run,
    sudo, hosts
)

env.hosts = ['locus.gilenofilho.com.br']
env.user = 'root'
env.program_name = 'ecommerce'
env.app_path = '/webapps/ecommerce/ecommerce/'
env.virtualenv_path = '/webapps/ecommerce/'

def start():
    print 'Starting ecommerce...'
    with cd(env.app_path):
        run("supervisorctl start %s" % env.program_name)
    print 'ecommerce is running'

def stop():
    print 'Stoping ecommerce...'
    with fabric_settings(warn_only=True):
        result = run("supervisorctl stop %s" % env.program_name)
        if result.failed:
            print '%s is not running' % env.program_name
            return result
    print '%s stopped successfully' % env.program_name
    return result

def deploy():
    stop()
    update_code()
    clear_pyc()
    update_virtualenv()
    collectstatic()
    update_database()
    start()

def update_code():
    with cd(env.app_path):
        run("git pull origin master")

def update_database():
    with cd(env.app_path):
        run("%sbin/python manage.py migrate" % (env.virtualenv_path))

def restart_nginx():
    run('service nginx restart')

def update_virtualenv():
    run("%sbin/pip install -r %srequirements.txt" % (env.virtualenv_path, env.app_path))

def createsuperuser():
    with cd(env.app_path):
        run("%sbin/python manage.py createsuperuser" % (env.virtualenv_path))

def loaddata(fixture):
    with cd(env.app_path):
        params = (env.virtualenv_path, fixture)
        run("%sbin/python manage.py loaddata %s" % params)

def clear_pyc():
    with cd(env.app_path):
        run('find . -name "*.pyc" -exec rm -rf {} \;')

def collectstatic():
    with cd(env.app_path):
        run("%sbin/python manage.py collectstatic --noinput" % (env.virtualenv_path))

def update_system():
    run('apt-get update')
    run('apt-get upgrade')
