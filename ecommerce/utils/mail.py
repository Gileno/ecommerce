# coding=utf-8

from django.core.mail import send_mail
from django.template import loader
from django.conf import settings


def send_mail_template(subject, to, template_name, context):
    message = loader.render_to_string(template_name, context)
    send_mail(
        subject, message, settings.DEFAULT_FROM_EMAIL,
        to
    )
