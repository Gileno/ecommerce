# coding=utf-8

from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings


urlpatterns = [
    url(r'^', include('apps.core.urls', namespace='core')),
    url(
        r'^catalogo/',
        include('apps.catalog.urls', namespace='catalog')
    ),
    url(
        r'^contas/',
        include('apps.accounts.urls', namespace='accounts')
    ),
    url(
        r'^compras/',
        include('apps.checkout.urls', namespace='checkout')
    ),
    url(r'^admin/', include(admin.site.urls)),
    url(
        r'^media/(?P<path>.*)$',
        'django.views.static.serve',
        {
            'document_root': settings.MEDIA_ROOT,
            'show_indexes': True
        }
    ),
]
